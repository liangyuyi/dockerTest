package com.liangyuyi.dockertest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
public class DockerTestApplication extends SpringBootServletInitializer {
	@Override
	protected SpringApplicationBuilder configure(
			SpringApplicationBuilder springApplicationBuilder) {
		springApplicationBuilder.sources(this.getClass());
		return super.configure(springApplicationBuilder);
	}

	public static void main(String[] args) {
		SpringApplication.run(DockerTestApplication.class, args);
	}
}
