package com.liangyuyi.dockertest.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author liangyuyi
 * @date 2018/8/13
 */
@RestController
public class TestController {
    @RequestMapping("response")
    public String response () {
        return "response success";
    }
}